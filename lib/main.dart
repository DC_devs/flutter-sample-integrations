import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'dart:io';
import 'dart:async';
import 'package:flutter_auth0/flutter_auth0.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:admob_flutter/admob_flutter.dart';

//final String clientId = 'ZCPA05BGia2U36tA2BE7A0x5xxpNiN3C';
final String clientId = '7FnfbekfG1VkId0u3jvWhDkkToX4qBf3';
final String domain = 'mocaiv.auth0.com';
AdmobBannerSize bannerSize;

final Auth0 auth = new Auth0(clientId: clientId, domain: domain);
final WebAuth web = new WebAuth(clientId: clientId, domain: domain);

void main() {
   Admob.initialize(getAppId());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Auth0 Integration',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
      ),
      // home: MyHomePage(title: 'Flutter layout demo'),
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) =>MyHomePage(title: 'Auth0 Integration'),
        // When navigating to the "/first" route, build the SecondScreen widget.
        '/first': (context) => FirstRoute(),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/second': (context) => SecondRoute(),
      },
    );
  }

}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<String> _userAcctData_uid;
  Future<int> _counter;

  Future<void> _setUserAcctData(String uid) async {
    final SharedPreferences prefs = await _prefs;
//    final Map<String, dynamic> userData_uid = (prefs.get('uid') ?? '');
    final String userData_uid = (prefs.getString('uid') ?? '');
//    final String userId = (prefs.get('userid') ?? '');
//    final String userEmail = (prefs.get('userEmail') ?? '');
    print(' ** uid ** '+userData_uid);
    setState(() {
      _userAcctData_uid = prefs.setString('uid', userData_uid).then((bool success) {
        return userData_uid;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    bannerSize = AdmobBannerSize.BANNER;
    _userAcctData_uid = _prefs.then((SharedPreferences prefs) {
      print('uid on init = '+(prefs.getString('uid') ?? ''));
      return (prefs.getString('uid') ?? '');
    });
  }

  bool _active = false;
  bool _active1 = false;

  String wRefreshToken;
  final GlobalKey<ScaffoldState> skey = GlobalKey<ScaffoldState>();

  void webLogin() {
    web.authorize(
      audience: 'https://$domain/userinfo',
      scope: 'openid email offline_access',
    ).then((value) {
      print('response: $value'+' -- access token '+value['access_token']);
      wRefreshToken = value['refresh_token'];
      runRegisterFuture(value['access_token']);
      // _userInfo(wRefreshToken);
    }).catchError((err) => print('Error: $err'));
  }

  // Function to call future
  void runRegisterFuture(String accessToken) {
    _userInfo(accessToken).then((value) {
      // Run extra code here
      print(' _userInfo *future* return '+value[0].values.toString());
      // TODO add update shared pref
      _setUserAcctData(value[0].values.toString());
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => FirstRoute()),
        );
    }, onError: (error) {
      print(' _userInfo *future* error '+error.toString());
    });
  }

  Future<List<Map<String, dynamic>>> _userInfo(String accessToken) async {
    List<Map<String, dynamic>> send=[] ;
    dynamic response = await auth.userInfo(token: accessToken);
    // StringBuffer buffer = new StringBuffer();
    // response.forEach((k, v) => buffer.writeln('$k: $v'));
    void iterateMapEntry(key, value) {
      // send[key] = value;
      Map<String, dynamic> myObject = {key: value} ;
      send.add(myObject);
      print('$key:$value'+' |**| send == '+send.toString());//string interpolation in action
    }
    response.forEach((k, v) => iterateMapEntry(k,v));
    return send;
//    return '''[User Info]
//    ${send.toString()}''';
  }

  void webRefreshToken() {
    if (wRefreshToken == null) {
      skey.currentState.showSnackBar(SnackBar(
        content: Text('Invalid Refresh Token'),
      ));
      return;
    }
    web
        .refreshToken(refreshToken: wRefreshToken)
        .then((value) => print('response: $value'))
        .catchError((err) => print('Error: $err'));
  }

  void _handleTapboxChanged(bool newValue) {
    setState(() {
      _active = newValue;
      if (_active) {

        webRefreshToken();

      }
    });
  }

  void _handleTapboxChanged1(bool newValue) {
    setState(() {
      _active1 = newValue;
      if (_active1) {
        // Navigator.pushNamed(context, '/first');
        webLogin();
      }
    });
  }

  Widget titleSection = Container(
    padding: const EdgeInsets.all(24),
    child: Row(
      children: [
        Expanded(
          /*1*/
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /*2*/
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Laneir Lake Campground',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(
                'Cold AF! Kandersteg, Switzerland',
                style: TextStyle(
                  color: Colors.red[700],
                ),
              ),
            ],
          ),
        ),
        /*3*/
        FavoriteWidget(),
//        Icon(
//          Icons.star,
//          color: Colors.red[500],
//        ),
//        Text('42'),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {

//    Column _buildButtonColumn(Color color, IconData icon, String label) {
//      return Column(
//        mainAxisSize: MainAxisSize.min,
//        mainAxisAlignment: MainAxisAlignment.center,
//        children: [
//          Icon(icon, color: color),
//          Container(
//            margin: const EdgeInsets.only(top: 8),
//            child: Text(
//              label,
//              style: TextStyle(
//                fontSize: 12,
//                fontWeight: FontWeight.w400,
//                color: color,
//              ),
//            ),
//          ),
//        ],
//      );
//    }
//
////    Color color = Theme.of(context).primaryColor;
////
////    Widget buttonSection = Container(
////      child: Row(
////        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
////        children: [
////          _buildButtonColumn(color, Icons.call, 'CALL'),
////          _buildButtonColumn(color, Icons.near_me, 'ROUTE'),
////          _buildButtonColumn(color, Icons.share, 'SHARE'),
////        ],
////      ),
////    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(24),
      child: Text(
        'Enter Auth0 User Info',
        softWrap: true,
      ),
    );

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: ListView( // Column(
        children: [
          Image.asset(
              'images/lake.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
          ),
          // titleSection,
          // buttonSection,
          textSection,
          MyStatefulWidget(),
          // TapboxA(),
          // TapboxB(
          //   active: _active,
          //   onChanged: _handleTapboxChanged,
          // ),
          FutureBuilder<String>(
              future: _userAcctData_uid,
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return const CircularProgressIndicator();
                  default:
                    if (snapshot.hasError)
                      return Text('Error: ${snapshot.error}');
                    else
                      return Text(
                        'User Id : ${snapshot.data}.\n\n'
                            'This should persist across restarts.',
//                        'User Id : ${snapshot.data} ${snapshot.data == '' ? '' : 's'}.\n\n'
//                            'This should persist across restarts.',
                      );
                }
              }),
          TapboxC(
            active: _active1,
            onChanged: _handleTapboxChanged1,
          ),
        ],
      ),
    );
  }

}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = true;
  int _favoriteCount = 40;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFavorited ? Icon(Icons.star) : Icon(Icons.star_border)),
            color: Colors.red[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$_favoriteCount'),
          ),
        ),
      ],
    );
  }

  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      } else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }
}

// TapboxA manages its own state.

//------------------------- TapboxA ----------------------------------

class TapboxA extends StatefulWidget {
  TapboxA({Key key}) : super(key: key);

  @override
  _TapboxAState createState() => _TapboxAState();
}

class _TapboxAState extends State<TapboxA> {
  bool _active = false;

  void _handleTap() {
    setState(() {
      _active = !_active;
      if (_active) {
        Navigator.pushNamed(context, '/second');
      }
    });
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handleTap,
      child: Container(
        child: Center(
          child: Text(
            _active ? 'Active' : 'Inactive',
            style: TextStyle(fontSize: 32.0, color: Colors.purple),
          ),
        ),
        width: 200.0,
        height: 200.0,
        decoration: BoxDecoration(
          color: _active ? Colors.lightGreenAccent[700] : Colors.blueGrey[600],
        ),
      ),
    );
  }
}

//------------------------- TapboxB ----------------------------------

class TapboxB extends StatelessWidget {
  TapboxB({Key key, this.active: false, @required this.onChanged})
      : super(key: key);

  final bool active;
  final ValueChanged<bool> onChanged;

  void _handleTap() {
    onChanged(!active);
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handleTap,
      child: Container(
        padding: const EdgeInsets.all(32),
        child: Center(
          child: Text(
            active ? 'Active' : 'Inactive',
            style: TextStyle(fontSize: 32.0, color: Colors.white),
          ),
        ),
        width: 200.0,
        height: 200.0,
        decoration: BoxDecoration(
          color: active ? Colors.lightGreen[700] : Colors.grey[600],
        ),
      ),
    );
  }
}

//----------------------------- TapboxC ------------------------------

class TapboxC extends StatefulWidget {
  TapboxC({Key key, this.active: false, @required this.onChanged})
      : super(key: key);

  final bool active;
  final ValueChanged<bool> onChanged;

  _TapboxCState createState() => _TapboxCState();
}

class _TapboxCState extends State<TapboxC> {
  bool _highlight = false;

  void _handleTapDown(TapDownDetails details) {
    setState(() {
      _highlight = true;
    });
  }

  void _handleTapUp(TapUpDetails details) {
    setState(() {
      _highlight = false;
    });
  }

  void _handleTapCancel() {
    setState(() {
      _highlight = false;
    });
  }

  void _handleTap() {
    widget.onChanged(!widget.active);
  }

  Widget build(BuildContext context) {
    // This example adds a green border on tap down.
    // On tap up, the square changes to the opposite state.
    return GestureDetector(
      onTapDown: _handleTapDown, // Handle the tap events in the order that
      onTapUp: _handleTapUp, // they occur: down, up, tap, cancel
      onTap: _handleTap,
      onTapCancel: _handleTapCancel,
      child: Container(
        child: Center(
          child: Text(widget.active ? '_REGISTERED_' : '*AccessRegister*',
              style: TextStyle(fontSize: 32.0, color: Colors.black)),
        ),
        width: 100.0,
        height: 200.0,
        decoration: BoxDecoration(
          color:
          widget.active ? Colors.lime[700] : Colors.grey[600],
          border: _highlight
              ? Border.all(
            color: Colors.indigo[700],
            width: 10.0,
          )
              : null,
        ),
      ),
    );
  }
}

class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Route'),
      ),
      body: Center(
//        child: RaisedButton(
//          child: Text('GO BACK'),
//          onPressed: () {
//            // Navigate to second route when tapped.
//            Navigator.pop(context);
//          },
//        ),
        child:Container(
          margin: EdgeInsets.only(bottom: 20.0),
          child: AdmobBanner(
            adUnitId: getBannerAdUnitId(),
            adSize: bannerSize,
            listener:
                (AdmobAdEvent event, Map<String, dynamic> args) {
              handleEvent(event, args, 'Banner');
            },
          ),
        ),
      ),
    );
  }

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        //showSnackBar('New Admob $adType Ad loaded!');
        print('New Admob $adType Ad loaded!');
        break;
      case AdmobAdEvent.opened:
        print('Admob $adType Ad opened!');
        break;
      case AdmobAdEvent.closed:
        print('Admob $adType Ad closed!');
        break;
      case AdmobAdEvent.failedToLoad:
        print('Admob $adType failed to load. :(');
        break;
      case AdmobAdEvent.rewarded:
        print('Admob rewareded. :(');
//        showDialog(
//          context: scaffoldState.currentContext,
//          builder: (BuildContext context) {
//            return WillPopScope(
//              child: AlertDialog(
//                content: Column(
//                  mainAxisSize: MainAxisSize.min,
//                  children: <Widget>[
//                    Text('Reward callback fired. Thanks Andrew!'),
//                    Text('Type: ${args['type']}'),
//                    Text('Amount: ${args['amount']}'),
//                  ],
//                ),
//              ),
//              onWillPop: () async {
//                scaffoldState.currentState.hideCurrentSnackBar();
//                return true;
//              },
//            );
//          },
//        );
        break;
      default:
    }
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            // Navigate back to first route when tapped.
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  final _formKey = GlobalKey<FormState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<String> _userAcctData_uid;
  // Create a text controller and use it to retrieve the current value
  // of the TextField.
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  String activeAuth = "";

  Future<String> _message = Future<String>.value('');

  void assignFuture(Function func) {
    setState(() {
      _message = func();
    });
  }

  Future<String> _signIn() async {
    try {
      print('-_signIN- :: username - password '+usernameController.text+'-'+passwordController.text);
      Auth0User user = await auth.passwordRealm(
          username: usernameController.text,
          password: passwordController.text,
          realm: 'Username-Password-Authentication',
          audience: '');
      return '''${user.accessToken}''';
    } catch (e) {
      return e;
    }
  }

  // Function to call future
  void runSignInFuture() {
    print(' ** run sign in *future*  ** ');
    _signIn().then((value) {
      // Run extra code here
      print(' sign IN *future* return '+value.toString());
      runRegisterFuture(value);
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Sign IN SUCCESS \n\n UserName: ' + usernameController.text + " \n PassWord: " + passwordController.text)));
    }, onError: (error) {
      print(' sign IN *future* error '+error.toString());
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Sign IN FAILURE \n\n UserName: ' + usernameController.text + " \n PassWord: " + passwordController.text)));

    });
  }

  Future<dynamic> _signUp() async {
    print('-_signUP- :: username - password '+usernameController.text+'-'+passwordController.text);
    try {
      dynamic user = await auth.createUser(
          email: usernameController.text,
          password: passwordController.text,
          connection: 'Username-Password-Authentication',
          waitResponse: true);
      return user;
    } catch (e) {
      return e;
    }
  }

  void runSignUpFuture() {
    _signUp().then((value) {
      // Run extra code here
      print(' sign UP *future* return '+value.toString());
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Sign UP SUCCESS \n\n UserName: ' + usernameController.text + " \n PassWord: " + passwordController.text)));
    }, onError: (error) {
      print(' sign UP *future* error '+error.toString());
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Sign UP FAILURE \n\n UserName: ' + usernameController.text + " \n PassWord: " + passwordController.text)));
    });
  }

  // Function to call future
  void runRegisterFuture(String accessToken) {
    _userInfo(accessToken).then((value) {
      // Run extra code here
      print(' _userInfo *future* return '+value[0].values.toString());
      // TODO add update shared pref
      _setUserAcctData(value[0].values.toString());
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => FirstRoute()),
      );
    }, onError: (error) {
      print(' _userInfo *future* error '+error.toString());
    });
  }

  Future<List<Map<String, dynamic>>> _userInfo(String accessToken) async {
    List<Map<String, dynamic>> send=[] ;
    dynamic response = await auth.userInfo(token: accessToken);
    // StringBuffer buffer = new StringBuffer();
    // response.forEach((k, v) => buffer.writeln('$k: $v'));
    void iterateMapEntry(key, value) {
      // send[key] = value;
      Map<String, dynamic> myObject = {key: value} ;
      send.add(myObject);
      print('$key:$value'+' |**| send == '+send.toString());//string interpolation in action
    }
    response.forEach((k, v) => iterateMapEntry(k,v));
    return send;
//    return '''[User Info]
//    ${send.toString()}''';
  }

  Future<void> _setUserAcctData(String uid) async {
    final SharedPreferences prefs = await _prefs;
//    final Map<String, dynamic> userData_uid = (prefs.get('uid') ?? '');
    final String userData_uid = (prefs.getString('uid') ?? '');
//    final String userId = (prefs.get('userid') ?? '');
//    final String userEmail = (prefs.get('userEmail') ?? '');

    print(' ** uid ** '+userData_uid);

    setState(() {
      _userAcctData_uid = prefs.setString('uid', userData_uid).then((bool success) {
        return userData_uid;
      });
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void _changeAuthStatus(String authBtn) {
    print(' *before* _changeAuthStatus called ** authBtn == '+authBtn);
    switch (authBtn) {
      case "REGISTER":
        activeAuth = 'register';
        break;
      case "LOGIN":
        activeAuth = 'login';
        break;
      case "FORGOT PASSWORD":
        activeAuth = 'password';
        break;
      default:
        break;
    }

    print(' *after* _changeAuthStatus called ** authBtn == '+activeAuth);
  }

  void _checkAuthStatusAndValidate() {
    print(' *before* _checkAuthStatusAndValidate called ** curr activeAuth == '+activeAuth);
    switch (activeAuth) {
      case "register":
        print(' register user == '+activeAuth);
        runSignUpFuture();
        break;
      case "login":
        print(' login user == '+activeAuth);
        runSignInFuture();
        break;
      case "password":
        print(' user forgot password == '+activeAuth);

        break;
      default:
        break;
    }

    print(' *after* _checkAuthStatusAndValidate called ** authBtn == '+activeAuth);
  }

  @override
  Widget build(BuildContext context) {

    Column _registerButtonColumn(Color color, IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            icon: Icon(icon),
            color: color,
            onPressed:() {
              _changeAuthStatus(label);
            },
          ),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: color,
              ),
            ),
          ),
        ],
      );
    }

    Widget auth0ButtonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _registerButtonColumn(Colors.indigo, Icons.add_box, 'REGISTER'),
          _registerButtonColumn(Colors.red, Icons.near_me, 'LOGIN'),
          _registerButtonColumn(Colors.green, Icons.share, 'FORGOT PASSWORD'),
        ],
      ),
    );

    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: TextFormField(
              controller: usernameController,
              decoration: InputDecoration(
                  labelText:"Mocaiv Username",
                  hintText: "Username"
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: TextFormField(
              controller: passwordController,
              decoration: InputDecoration(
                  labelText:"Mocaiv Password",
                  hintText: "Password"
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0,horizontal: 24.0),
              child:auth0ButtonSection,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0,horizontal: 24.0),
            child: RaisedButton(
              onPressed: () {
                // Validate will return true if the form is valid, or false if
                // the form is invalid.


                // activeAuth

                if (_formKey.currentState.validate()) {
                  _checkAuthStatusAndValidate();
                }
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}

String getAppId() {
  if (Platform.isIOS) {
    return 'ca-app-pub-2207635497498310~1459995176';
  } else if (Platform.isAndroid) {
    return 'ca-app-pub-2207635497498310~1459995176';
  }
  return null;
}

String getBannerAdUnitId() {
  if (Platform.isIOS) {
    return 'ca-app-pub-2207635497498310/3591334495';
  } else if (Platform.isAndroid) {
    return 'ca-app-pub-2207635497498310/2530314516';
  }
  return null;
}
